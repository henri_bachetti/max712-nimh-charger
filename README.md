# MAX712 NI-MH CHARGER

The purpose of this page is to explain step by step the realization of a NI-MH battery charger.

The charger uses the following components :

 * a MAX712
 * a TIP42
 * a regulator LM317T
 * a transistor 2N2222
 * a transistor 2N5087
 * some passive components
 * a power supply

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/09/chargeur-de-batterie-ni-mh.html

